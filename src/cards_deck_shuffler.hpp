#pragma once
#include <numeric>
#include <vector>

static const unsigned int DEFAULT_SINGLE_DECK_SIZE = 313;

class CardsDeckShuffler {
public:
  CardsDeckShuffler(unsigned int number_of_cards,
                    unsigned int single_deck_size = DEFAULT_SINGLE_DECK_SIZE)
      : number_of_cards_(number_of_cards), single_deck_size_(single_deck_size) {
    deck_initial_.reserve(number_of_cards_);
    for (unsigned int i = 0; i < number_of_cards_; ++i) {
      deck_initial_.push_back(i % single_deck_size_);
    }
  }

  static void shuffleRound(std::vector<unsigned short> &deck) {
    std::vector<unsigned short> table(deck.size(), 0);
    size_t table_index = table.size() - 1;
    bool put_to_table = true;
    for (size_t i = 0; i < deck.size(); ++i) {
      auto card = deck[i];
      if (put_to_table) {
        table[table_index--] = card;
      } else {
        deck.push_back(card);
      }
      put_to_table = !put_to_table;
    }
    std::swap(deck, table);
  }

  unsigned long long shuffleUntilTheSameNaive() {
    std::vector<unsigned short> deck = deck_initial_;
    if (deck.size() <= 1) {
      return 0;
    }
    unsigned long long count = 0;
    do {
      count++;
      shuffleRound(deck);
    } while (deck != deck_initial_);
    return count;
  }

  unsigned long long shuffleUntilTheSameFast() {
    std::vector<unsigned short> deck = deck_initial_;
    shuffleRound(deck);
    std::vector<unsigned short> group(deck.size(), 1);
    for (size_t i = 0; i < deck.size(); ++i) {
      size_t index = i;
      while (deck[index] != i) {
        index = deck[index];
        group[i]++;
      }
    }
    return lcm(group);
  }

private:
  // Computes the least common multiple
  unsigned long long lcm(const std::vector<unsigned short> &deck) {
    unsigned long long p = 1;
    for (size_t i = 0; i < deck.size(); ++i) {
      p = p * int(deck[i] / std::gcd(p, deck[i]));
    }
    return p;
  }

  const unsigned int number_of_cards_;
  const unsigned int single_deck_size_;
  std::vector<unsigned short> deck_initial_;
};
