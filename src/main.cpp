#include <iostream>
#include <vector>

#include "cards_deck_shuffler.hpp"

static const unsigned int MAXIMUM_INPUT_NUMBER_OF_CARDS = 313;

struct Input {
  unsigned int number_of_cards;
};
struct Output {
  unsigned long long number_of_rounds;
};

void parseInput(Input &input, int argc, char **argv) {
  if (argc < 2) {
    const std::string exec = argc ? argv[0] : "executable";
    throw std::runtime_error("Not all parameters are provided. Usage: " + exec +
                             " NUMBER_OF_CARDS");
  }

  const long number_of_cards = strtol(argv[1], 0, 10);
  if (number_of_cards < 1 || number_of_cards > MAXIMUM_INPUT_NUMBER_OF_CARDS) {
    throw std::runtime_error("NUMBER_OF_CARDS has the wrong value");
  }

  input = Input{static_cast<unsigned int>(number_of_cards)};
}

void writeOutput(const Output &output) {
  std::cout << output.number_of_rounds << std::endl;
}

int main(int argc, char **argv) {
  try {
    Input input;
    parseInput(input, argc, argv);

    CardsDeckShuffler game(input.number_of_cards);
    unsigned long long number_of_rounds = game.shuffleUntilTheSameFast();

    writeOutput(Output{number_of_rounds});

  } catch (const std::exception &e) {
    std::cerr << "[ERROR]: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
