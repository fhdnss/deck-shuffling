from .utils import *
import pytest


@pytest.mark.parametrize("number_of_cards, expected_result", [
	(1, 1),
	(3, 3),
	(4, 2),
])
def test_happy(request, number_of_cards, expected_result):
	(output, err, code) = run_executable([
		request.config.option.binary_path, 
		number_of_cards
	])
	assert code == 0
	result = int(output.decode('utf-8'))
	assert result == expected_result
