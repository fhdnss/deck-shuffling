import os
from random import random
import subprocess
import json


def run_executable(params):
	params = [str(i) for i in params]
	p = subprocess.Popen(params, stdout=subprocess.PIPE)
	(output, err) = p.communicate()
	code = p.wait()
	return (output, err, code)
