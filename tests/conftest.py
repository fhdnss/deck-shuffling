import pytest

def pytest_addoption(parser):
    parser.addoption("--binary_path", action="store")
