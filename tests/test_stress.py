from .utils import *
import pytest


def dummy_shuffle_round(deck):
	table = []
	while deck:
		table.append(deck[0])
		del deck[0]
		if deck:
			deck.append(deck[0])
			del deck[0]
	return list(reversed(table))


def dummy_shuffle_until_the_same(number_of_cards):
	deck = list(range(0, number_of_cards))
	deck_init = deck.copy()
	count = 0
	while True:
		count += 1
		deck = dummy_shuffle_round(deck)
		if deck == deck_init:
			break
	return count


@pytest.mark.parametrize("number_of_cards", list(range(1, 180)))
def test_stress(request, number_of_cards):
	(output, err, code) = run_executable([
		request.config.option.binary_path, 
		number_of_cards
	])
	assert code == 0
	result = int(output.decode('utf-8'))
	expected_result = dummy_shuffle_until_the_same(number_of_cards)
	assert result == expected_result
