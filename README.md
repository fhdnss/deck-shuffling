# deck_shuffling

You are given a deck containing 313 cards. While holding the deck:
1. Take the top card off the deck and set it on the table
2. Take the next card off the top and put it on the bottom of the deck in your hand.
3. Continue steps 1 and 2 until all cards are on the table. This is a round. 
4. Pick up the deck from the table and repeat steps 1-3 until the deck is in the original order.
Here is the program to determine how many rounds it will take to put the deck back into the original order. It takes a number of cards in the deck as a command line argument and write the result to stdout.

### Build

```sh
$ make build
```

##### Build Dependencies
* [cmake](https://cmake.org/)

### Tests

```sh
$ make tests
```

##### Tests Dependencies
* [python3+](https://www.python.org/)
* [pytest](https://docs.pytest.org/en/stable/)

### Usage

##### Run:
```sh
$ BINARY NUMBER_OF_CARDS
```
- **BINARY** - path to the utility executable file
- **NUMBER_OF_CARDS** - number of cards in the deck

###### Example:
```sh
./build/deck_shuffling 313
```

###### Example output:
```sh
1575169365
```
- number of rounds

License
----

MIT
