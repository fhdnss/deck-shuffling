default_target: build

BUILD_DIR = build
SRC_DIR = src
BINARY_NAME = deck_shuffling

build:
	mkdir -p $(BUILD_DIR) && cd $(BUILD_DIR) && cmake ../$(SRC_DIR) && make

tests: build
	cd tests && pytest --binary_path=../$(BUILD_DIR)/$(BINARY_NAME)

clean:
	rm -rf $(BUILD_DIR)
